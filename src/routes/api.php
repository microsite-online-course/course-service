<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('mentor')->group(function () {
        Route::get("/", "Api\MentorController@index");
        Route::get("/show/{uuid}", "Api\MentorController@show");
        Route::post("/", "Api\MentorController@store");
        Route::put("update/{uuid}", "Api\MentorController@update");
        Route::delete("/{uuid}", "Api\MentorController@destroy");
    });

    Route::prefix('course')->group(function () {
        Route::get("/", "Api\CourseController@index");
        Route::get("/show/{uuid}", "Api\CourseController@show");
        Route::post("/", "Api\CourseController@store");
        Route::put("update/{uuid}", "Api\CourseController@update");
        Route::delete("delete/{uuid}", "Api\CourseController@destroy");
    });

    Route::prefix('chapter')->group(function () {
        Route::get("/", "Api\ChapterController@index");
        Route::get("/show/{uuid}", "Api\ChapterController@show");
        Route::post("/", "Api\ChapterController@store");
        Route::put("update/{uuid}", "Api\ChapterController@update");
        Route::delete("delete/{uuid}", "Api\ChapterController@destroy");
    });

    Route::prefix('lesson')->group(function () {
        Route::get("/", "Api\LessonController@index");
        Route::get("/show/{uuid}", "Api\LessonController@show");
        Route::post("/", "Api\LessonController@store");
        Route::put("update/{uuid}", "Api\LessonController@update");
        Route::delete("delete/{uuid}", "Api\LessonController@destroy");
    });

    Route::prefix('image-course')->group(function () {
        Route::get("/", "Api\ImageCourseController@index");
        Route::get("/show/{uuid}", "Api\ImageCourseController@show");
        Route::post("/", "Api\ImageCourseController@store");
        Route::put("update/{uuid}", "Api\ImageCourseController@update");
        Route::delete("delete/{uuid}", "Api\ImageCourseController@destroy");
    });

    Route::prefix('my-course')->group(function () {
        Route::get("/", "Api\MyCourseController@index");
        Route::get("/show/{uuid}", "Api\MyCourseController@show");
        Route::post("/", "Api\MyCourseController@store");
        Route::post("/premium", "Api\MyCourseController@storePremiumAccess");
        Route::put("update/{uuid}", "Api\MyCourseController@update");
        Route::delete("delete/{uuid}", "Api\MyCourseController@destroy");
    });

    Route::prefix('review')->group(function () {
        Route::get("/", "Api\ReviewController@index");
        Route::get("/show/{uuid}", "Api\ReviewController@show");
        Route::post("/", "Api\ReviewController@store");
        Route::put("update/{uuid}", "Api\ReviewController@update");
        Route::delete("delete/{uuid}", "Api\ReviewController@destroy");
    });
});
