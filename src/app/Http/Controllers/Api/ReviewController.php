<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Models\Review;
use App\Models\Course;


class ReviewController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "course_uuid" => "uuid"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $q = $req->q ?? null;
            $course_uuid = $req->course_uuid ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $myCourses = Review::with(["course" => function ($query) {
                $query->select(["id", "uuid", "name", "created_at", "updated_at"]);
            }])
                ->when($course_uuid, function ($query) use ($course_uuid) {
                    $query->whereHas("course", function ($query2) use ($course_uuid) {
                        $query2->where("uuid", $course_uuid);
                    });
                })
                ->select([
                    'course_id', "uuid", 'user_uuid', "rating", "note", "created_at", "updated_at"
                ])
                ->orderby("created_at", 'desc');

            $total = $myCourses->count();
            $result = $myCourses->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    $user = json_decode(json_encode(getServiceUser($row->user_uuid), true));
                    $userUuid = $user->data->user ? $user->data->user->uuid : null;
                    $userName = $user->data->user ? $user->data->user->name : null;

                    return [
                        "uuid" => $row->uuid,
                        "course" => [
                            "uuid" => $row->course->uuid,
                            "name" => $row->course->name,
                        ],
                        "user" => [
                            "uuid" => $userUuid,
                            "name" => $userName,
                        ],
                        "rating" => $row->rating,
                        "note" => $row->note,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $review = Review::with(["course" => function ($query) {
                $query->select(["id", "uuid", "name", "created_at", "updated_at"]);
            }])
                ->where('uuid', $uuid)
                ->first([
                    'uuid', 'course_id', 'user_uuid', "rating", "note", "created_at", "updated_at"
                ]);

            if (!$review) {
                return response()->json([
                    "status" => "error",
                    "message" => 'review not found'
                ], 404);
            }

            $user = json_decode(json_encode(getServiceUser($review->user_uuid), true));

            if ($user->status === "error") {
                return response()->json([
                    "status" => $user->status,
                    "message" => $user->message
                ], $user->http_code);
            }

            $userUuid = $user->data->user ? $user->data->user->uuid : null;
            $userName = $user->data->user ? $user->data->user->name : null;

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $review->uuid,
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "course" => [
                        "uuid" => $review->course->uuid,
                        "name" => $review->course->name,
                    ],
                    'rating' => (int) $review->rating,
                    'note' => $review->note,
                    "created_at" => $review->created_at,
                    "updated_at" => $review->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $rules = [
                "course_uuid" => "required|uuid",
                "user_uuid" => "required|uuid",
                "note" => "string",
                "rating" => "required|integer|min:1|max:5",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $user = json_decode(json_encode(getServiceUser($req->user_uuid), true));

            if ($user->status === "error") {
                return response()->json([
                    "status" => $user->status,
                    "message" => $user->message
                ], $user->http_code);
            }

            $userUuid = $user->data->user ? $user->data->user->uuid : null;
            $userName = $user->data->user ? $user->data->user->name : null;

            $isExists = Review::where([
                ['user_uuid', $userUuid],
                ['course_id', $course->id],
            ])
                ->exists();


            if ($isExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "review $course->name for user $userName already exists"
                ], 403);
            }

            $review = Review::create([
                'name' => $req->name,
                'note' => $req->note,
                'rating' => $req->rating,
                'uuid' => Str::uuid(),
                'course_id' => $course->id,
                'user_uuid' => $userUuid,
            ]);

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "course" => [
                        "uuid" => $review->course->uuid,
                        "name" => $review->course->name,
                    ],
                    "uuid" => $review->uuid,
                    "rating" => (int) $review->rating,
                    "note" => $review->note,
                    "created_at" => $review->created_at,
                    "updated_at" => $review->updated_at
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "course_uuid" => "required|uuid",
                "user_uuid" => "required|uuid",
                "note" => "string",
                "rating" => "integer",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $review = Review::where('uuid', $uuid)->first();

            if (!$review) {
                return response()->json([
                    "status" => "error",
                    "message" => 'review not found'
                ], 404);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $user = json_decode(json_encode(getServiceUser($req->user_uuid), true));

            if ($user->status === "error") {
                return response()->json([
                    "status" => $user->status,
                    "message" => $user->message
                ], $user->http_code);
            }

            $userUuid = $user->data->user ? $user->data->user->uuid : null;
            $userName = $user->data->user ? $user->data->user->name : null;

            $isExists = Review::where([
                ['user_uuid', $userUuid],
                ['course_id', $course->id],
                ['uuid', "!=", $uuid],
            ])
                ->exists();


            if ($isExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "review $course->name for user $userName already exists"
                ], 403);
            }

            if ($req->course_uuid) $review->course_id = $course->id;
            if ($req->user_uuid) $review->user_uuid = $userUuid;
            if ($req->rating) $review->rating = $req->rating;
            if ($req->note) $review->note = $req->note;

            $review->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "course" => [
                        "uuid" => $review->course->uuid,
                        "name" => $review->course->name,
                    ],
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "uuid" => $review->uuid,
                    "rating" => $review->rating,
                    "note" => $review->note,
                    "created_at" => $review->created_at,
                    "updated_at" => $review->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();

            $review = Review::where('uuid', $uuid)->first();

            if (!$review) {
                return response()->json([
                    "status" => "error",
                    "message" => 'review not found'
                ], 404);
            }

            $review->delete();

            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }
}
