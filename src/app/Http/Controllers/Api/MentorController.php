<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Models\Mentor;

class MentorController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $q = $req->q ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $mentors = Mentor::select([
                'uuid', 'name', 'profile', 'email', 'profession', "created_at", "updated_at"
            ])
                ->when($q, function ($query) use ($q) {
                    $query->whereRaw("lower(name) like '%$q%'");
                })
                ->orderby("created_at", 'desc');

            $total = $mentors->count();
            $result = $mentors->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => $result
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $mentor = Mentor::where('uuid', $uuid)
                ->first([
                    'uuid', 'profile', 'email', 'profession', "created_at", "updated_at"
                ]);

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => 'mentor not found'
                ], 404);
            }

            return response()->json([
                "status" => "success",
                "data" => $mentor
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $req)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "name" => "required|string",
                "profile" => "required|url",
                "profession" => "required|string",
                "email" => "required|email",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $isEmailExits = Mentor::where("email", $req->email)->exists();
            if ($isEmailExits) {
                return response()->json([
                    "status" => "error",
                    "message" => "email $req->email already exists"
                ], 400);
            }

            $mentor = Mentor::create([
                'name' => $req->name,
                'uuid' => Str::uuid(),
                'profile' => $req->profile,
                'email' => $req->email,
                'profession' => $req->profession
            ]);

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $mentor->uuid,
                    "name" => $mentor->name,
                    "profile" => $mentor->profile,
                    "email" => $mentor->email,
                    "profession" => $mentor->profession,
                    "created_at" => $mentor->created_at,
                    "updated_at" => $mentor->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();
            $rules = [
                "name" => "string",
                "profile" => "url",
                "profession" => "string",
                "email" => "email",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $mentor = Mentor::where('uuid', $uuid)->first();

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => 'mentor not found'
                ], 404);
            }

            $isEmailExists = Mentor::where([
                ['email', $req->email],
                ['uuid', '!=', $uuid],
            ])
                ->exists();

            if ($isEmailExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "email $req->email already exists"
                ], 403);
            }

            if ($req->name) $mentor->name = $req->name;
            if ($req->profile) $mentor->profile = $req->profile;
            if ($req->profession) $mentor->profession = $req->profession;
            if ($req->email) $mentor->email = $req->email;

            $mentor->save();

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $mentor->uuid,
                    "profile" => $mentor->profile,
                    "email" => $mentor->email,
                    "profession" => $mentor->profession,
                    "created_at" => $mentor->created_at,
                    "updated_at" => $mentor->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();

            $mentor = Mentor::where('uuid', $uuid)->first();

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => 'mentor not found'
                ], 404);
            }

            $mentor->delete();
            DB::commit();

            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }
}
