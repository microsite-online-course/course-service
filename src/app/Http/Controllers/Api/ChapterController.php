<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Chapter;
use App\Models\Course;

class ChapterController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $q = $req->q ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $chapters = Chapter::with(['course' => function ($q) {
                $q->select('id', 'uuid', 'name');
            }])
                ->select([
                    'uuid', 'name', 'course_id', "created_at", "updated_at"
                ])
                ->when($q, function ($query) use ($q) {
                    $query->whereRaw("lower(name) like '$q'");
                })
                ->orderby("created_at", 'desc');

            $total = $chapters->count();
            $result = $chapters->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    return [
                        "uuid" => $row->uuid,
                        "course" => [
                            "uuid" => $row->course->uuid,
                            "name" => $row->course->name,
                        ],
                        "name" => $row->name,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "name" => "required|string",
                "course_uuid" => "required|uuid",
            ];

            $data = $request->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where("uuid", $request->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $chapter = Chapter::create([
                'name' => $request->name,
                'uuid' => Str::uuid(),
                "course_id" => $course->id
            ]);

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $chapter->uuid,
                    "name" => $chapter->name,
                    "created_at" => $chapter->created_at,
                    "updated_at" => $chapter->updated_at,
                    "course" => [
                        "uuid" => $chapter->course->uuid,
                        "name" => $chapter->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $chapter = Chapter::where('uuid', $uuid)->first();

            if (!$chapter) {
                return response()->json([
                    "status" => "error",
                    "message" => 'chapter not found'
                ], 404);
            }

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $chapter->uuid,
                    "name" => $chapter->name,
                    "created_at" => $chapter->created_at,
                    "updated_at" => $chapter->updated_at,
                    "course" => [
                        "uuid" => $chapter->course->uuid,
                        "name" => $chapter->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "name" => "required|string",
                "course_uuid" => "required|uuid",
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $chapter = Chapter::where('uuid', $uuid)->first();

            if (!$chapter) {
                return response()->json([
                    "status" => "error",
                    "message" => "chapter not found"
                ], 404);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            if ($req->name) $chapter->name = $req->name;
            if ($req->course_uuid) $course->course_id = $course->id;
            $chapter->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $chapter->uuid,
                    "name" => $chapter->name,
                    "created_at" => $course->created_at,
                    "updated_at" => $course->updated_at,
                    "course" => [
                        "uuid" => $chapter->course->uuid,
                        "name" => $chapter->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();
            $mentor = Chapter::where('uuid', $uuid)->first();

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => 'chapter not found'
                ], 404);
            }

            $mentor->delete();
            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }
}
