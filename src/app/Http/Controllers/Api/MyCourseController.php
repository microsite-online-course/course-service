<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Models\Course;
use App\Models\MyCourse;

class MyCourseController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "user_uuid" => "uuid"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $user_uuid = $req->user_uuid ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $myCourses = MyCourse::with(["course" => function ($query) {
                $query->select(["id", "uuid", "name", "created_at", "updated_at"]);
            }])
                ->select([
                    'course_id', 'uuid', 'user_uuid', "created_at", "updated_at"
                ])
                ->when($user_uuid, function ($query) use ($user_uuid) {
                    $query->where("user_uuid", $user_uuid);
                })
                ->orderby("created_at", 'desc');

            $total = $myCourses->count();
            $result = $myCourses->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    $user = json_decode(json_encode(getServiceUser($row->user_uuid), true));
                    $userUuid = $user->data->user ? $user->data->user->uuid : null;
                    $userName = $user->data->user ? $user->data->user->name : null;

                    return [
                        "uuid" => $row->uuid,
                        "course" => [
                            "uuid" => $row->course->uuid,
                            "name" => $row->course->name,
                        ],
                        "user" => [
                            "uuid" => $userUuid,
                            "name" => $userName,
                        ],
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $myCourse = MyCourse::with(["course" => function ($query) {
                $query->select(["id", "uuid", "name", "created_at", "updated_at"]);
            }])
                ->where('uuid', $uuid)
                ->first([
                    'uuid', 'course_id', 'user_uuid', "created_at", "updated_at"
                ]);

            if (!$myCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => 'my course not found'
                ], 404);
            }

            $user = json_decode(json_encode(getServiceUser($myCourse->user_uuid), true));

            if ($user->status === "error") {
                return response()->json([
                    "status" => $user->status,
                    "message" => $user->message
                ], $user->http_code);
            }

            $userUuid = $user->data->user ? $user->data->user->uuid : null;
            $userName = $user->data->user ? $user->data->user->name : null;

            return response()->json([
                "status" => "success",
                "data" => [
                    "course" => [
                        "uuid" => $myCourse->course->uuid,
                        "name" => $myCourse->course->name,
                    ],
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "uuid" => $myCourse->uuid,
                    "created_at" => $myCourse->created_at,
                    "updated_at" => $myCourse->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $req)
    {
        try {

            DB::beginTransaction();

            $rules = [
                "course_uuid" => "required|uuid",
                "user_uuid" => "required|uuid",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $user = getServiceUser($req->user_uuid);

            if ($user['status'] === "error") {
                return response()->json([
                    "status" => $user['status'],
                    "message" => $user['message']
                ], $user['http_code']);
            }

            $userUuid = $user['data'] ? ($user['data']['user'] ? $user['data']['user']['uuid'] : null) : null;
            $userName =  $user['data'] ? ($user['data']['user'] ? $user['data']['user']['name'] : null) : null;

            $isCourseExists = MyCourse::where([
                ['user_uuid', $userUuid],
                ['course_id', $course->id],
            ])
                ->exists();


            if ($isCourseExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "course $course->name for user $userName already exists"
                ], 403);
            }

            if ($course->type === "premium") {
                $order = postOrder([
                    "user" => $user['data'] ?  $user['data']['user'] : null,
                    "course" => $course->toArray(),
                ]);

                if ($order["status"] === "error") {
                    return response()->json([
                        "status" => $order['status'],
                        "message" => $order['message']
                    ], $order['http_code']);
                }

                return response()->json([
                    "status" => "success",
                    "data" => $order["data"]
                ]);
            } else {
                $myCourse = MyCourse::create([
                    'uuid' => Str::uuid(),
                    'course_id' => $course->id,
                    'user_uuid' => $userUuid,
                ]);

                DB::commit();
                return response()->json([
                    "status" => "success",
                    "data" => [
                        "user" => [
                            "uuid" => $userUuid,
                            "name" => $userName,
                        ],
                        "course" => [
                            "uuid" => $myCourse->course->uuid,
                            "name" => $myCourse->course->name,
                        ],
                        "uuid" => $myCourse->uuid,
                        "created_at" => $myCourse->created_at,
                        "updated_at" => $myCourse->updated_at
                    ]
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "message" => $e->getMessage()
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "course_uuid" => "required|uuid",
                "user_uuid" => "required|uuid",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $myCourse = MyCourse::where('uuid', $uuid)->first();

            if (!$myCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => 'my course not found'
                ], 404);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $user = json_decode(json_encode(getServiceUser($req->user_uuid), true));

            if ($user->status === "error") {
                return response()->json([
                    "status" => $user->status,
                    "message" => $user->message
                ], $user->http_code);
            }

            $userUuid = $user->data->user ? $user->data->user->uuid : null;
            $userName = $user->data->user ? $user->data->user->name : null;


            $isCourseExists = MyCourse::where([
                ['user_uuid', $userUuid],
                ['course_id', $course->id],
                ['uuid', '!=', $uuid],
            ])
                ->exists();

            if ($isCourseExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "course $course->name for user $userName already exists"
                ], 403);
            }

            if ($req->course_uuid) $myCourse->course_id = $course->id;
            if ($req->user_uuid) $myCourse->user_uuid = $userUuid;

            $myCourse->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $myCourse->uuid,
                    "created_at" => $myCourse->created_at,
                    "updated_at" => $myCourse->updated_at,
                    "course" => [
                        "uuid" => $myCourse->course->uuid,
                        "name" => $myCourse->course->name,
                    ],
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();

            $myCourse = MyCourse::where('uuid', $uuid)->first();

            if (!$myCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => 'my course not found'
                ], 404);
            }

            $myCourse->delete();

            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function storePremiumAccess(Request $req)
    {
        try {

            DB::beginTransaction();

            $rules = [
                "course_uuid" => "required|uuid",
                "user_uuid" => "required|uuid",
            ];

            $data = $req->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $user = getServiceUser($req->user_uuid);

            if ($user['status'] === "error") {
                return response()->json([
                    "status" => $user['status'],
                    "message" => $user['message']
                ], $user['http_code']);
            }

            $userUuid = $user['data'] ? ($user['data']['user'] ? $user['data']['user']['uuid'] : null) : null;
            $userName =  $user['data'] ? ($user['data']['user'] ? $user['data']['user']['name'] : null) : null;

            $isCourseExists = MyCourse::where([
                ['user_uuid', $userUuid],
                ['course_id', $course->id],
            ])
                ->exists();


            if ($isCourseExists) {
                return response()->json([
                    "status" => "error",
                    "message" => "course $course->name for user $userName already exists"
                ], 403);
            }


            $myCourse = MyCourse::create([
                'uuid' => Str::uuid(),
                'course_id' => $course->id,
                'user_uuid' => $userUuid,
            ]);

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "course" => [
                        "uuid" => $myCourse->course->uuid,
                        "name" => $myCourse->course->name,
                    ],
                    "uuid" => $myCourse->uuid,
                    "created_at" => $myCourse->created_at,
                    "updated_at" => $myCourse->updated_at
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "message" => $e
            ]);
        }
    }
}
