<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Models\Lesson;
use App\Models\Chapter;

class LessonController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "chapter_uuid" => "uuid"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $chapterUuid = $req->chapter_uuid ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $lessons = Lesson::with(['chapter' => function ($q) {
                $q->select('id', 'uuid', 'name');
            }])
                ->when($chapterUuid, function ($query) use ($chapterUuid) {
                    $query->whereHas("chapter", function ($query2) use ($chapterUuid) {
                        $query2->where("uuid", $chapterUuid);
                    });
                })
                ->select(['uuid', 'video', 'chapter_id', "created_at", "updated_at"])
                ->orderby("created_at", 'desc');

            $total = $lessons->count();
            $result = $lessons->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    return [
                        "uuid" => $row->uuid,
                        "chapter" => [
                            "uuid" => $row->chapter->uuid,
                            "name" => $row->chapter->name,
                        ],
                        "video" => $row->video,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $rules = [
                "video" => "required|string",
                "chapter_uuid" => "required|uuid",
            ];

            $data = $request->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $chapter = Chapter::where("uuid", $request->chapter_uuid)->first();

            if (!$chapter) {
                return response()->json([
                    "status" => "error",
                    "message" => "chapter not found"
                ], 404);
            }

            $lesson = Lesson::create([
                'video' => $request->video,
                'uuid' => Str::uuid(),
                "chapter_id" => $chapter->id
            ]);

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $lesson->uuid,
                    "video" => $lesson->video,
                    "created_at" => $lesson->created_at,
                    "updated_at" => $lesson->updated_at,
                    "chapter" => [
                        "uuid" => $lesson->chapter->uuid,
                        "name" => $lesson->chapter->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $lesson = Lesson::where('uuid', $uuid)->first();

            if (!$lesson) {
                return response()->json([
                    "status" => "error",
                    "message" => 'lesson not found'
                ], 404);
            }

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $lesson->uuid,
                    "video" => $lesson->video,
                    "created_at" => $lesson->created_at,
                    "updated_at" => $lesson->updated_at,
                    "chapter" => [
                        "uuid" => $lesson->chapter->uuid,
                        "name" => $lesson->chapter->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "chapter_uuid" => "required|uuid",
                "video" => "string",
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $lesson = Lesson::where('uuid', $uuid)->first();

            if (!$lesson) {
                return response()->json([
                    "status" => "error",
                    "message" => "lesson not found"
                ], 404);
            }

            $chapter = Chapter::where("uuid", $req->chapter_uuid)->first();

            if (!$chapter) {
                return response()->json([
                    "status" => "error",
                    "message" => "mentor not found"
                ], 404);
            }

            if ($req->video) $lesson->video = $req->video;
            if ($req->chapter_uuid) $lesson->chapter_id = $chapter->id;

            $lesson->save();

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $lesson->uuid,
                    "video" => $lesson->video,
                    "created_at" => $lesson->created_at,
                    "updated_at" => $lesson->updated_at,
                    "chapter" => [
                        "uuid" => $lesson->chapter->uuid,
                        "name" => $lesson->chapter->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();

            $lesson = Lesson::where('uuid', $uuid)->first();

            if (!$lesson) {
                return response()->json([
                    "status" => "error",
                    "message" => 'lesson not found'
                ], 404);
            }

            $lesson->delete();

            DB::commit();

            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }
}
