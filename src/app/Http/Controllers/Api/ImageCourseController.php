<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use App\Models\ImageCourse;
use App\Models\Course;

class ImageCourseController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "course_uuid" => "uuid",
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $courseUuid = $req->course_uuid ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $imageCourse = ImageCourse::with(['course' => function ($q) {
                $q->select('id', 'uuid', 'name');
            }])
                ->select(['uuid', 'image', 'course_id', "created_at", "updated_at"])
                ->when($courseUuid, function ($query) use ($courseUuid) {
                    $query->whereHas("course", function ($query2) use ($courseUuid) {
                        $query2->where("uuid", $courseUuid);
                    });
                })
                ->orderby("created_at", 'desc');

            $total = $imageCourse->count();
            $result = $imageCourse->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    return [
                        "uuid" => $row->uuid,
                        "course" => [
                            "uuid" => $row->course->uuid,
                            "name" => $row->course->name,
                        ],
                        "image" => $row->image,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $req)
    {
        try {
            DB::beginTransaction();
            $rules = [
                "image" => "required|url",
                "course_uuid" => "required|uuid"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $imageCourse = ImageCourse::create([
                'image' => $req->image,
                'uuid' => Str::uuid(),
                "course_id" => $course->id,
            ]);

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $imageCourse->uuid,
                    "image" => $imageCourse->image,
                    "created_at" => $imageCourse->created_at,
                    "updated_at" => $imageCourse->updated_at,
                    "course" => [
                        "uuid" => $imageCourse->course->uuid,
                        "name" => $imageCourse->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show(Request $req, $uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $imageCourse = ImageCourse::where('uuid', $req->uuid)->first();

            if (!$imageCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => 'image course not found'
                ], 404);
            }

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $imageCourse->uuid,
                    "image" => $imageCourse->image,
                    "created_at" => $imageCourse->created_at,
                    "updated_at" => $imageCourse->updated_at,
                    "course" => [
                        "uuid" => $imageCourse->course->uuid,
                        "name" => $imageCourse->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "image" => "url",
                "course_uuid" => "required|uuid",
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $imageCourse = ImageCourse::where('uuid', $uuid)->first();

            if (!$imageCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => "image course not found"
                ], 404);
            }

            $course = Course::where("uuid", $req->course_uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            if ($req->image) $imageCourse->image = $req->image;
            if ($req->course_uuid) $imageCourse->course_id = $course->id;

            $imageCourse->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $imageCourse->uuid,
                    "image" => $imageCourse->image,
                    "created_at" => $imageCourse->created_at,
                    "updated_at" => $imageCourse->updated_at,
                    "course" => [
                        "uuid" => $imageCourse->course->uuid,
                        "name" => $imageCourse->course->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();
            $imageCourse = ImageCourse::where('uuid', $uuid)->first();

            if (!$imageCourse) {
                return response()->json([
                    "status" => "error",
                    "message" => 'image course not found'
                ], 404);
            }

            $imageCourse->delete();

            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }
}
