<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Models\Course;
use App\Models\Mentor;

class CourseController extends Controller
{
    public function index(Request $req)
    {
        try {
            $rules = [
                "page" => "integer",
                "status" => "string"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $page = $req->page ?? 1;
            $q = $req->q ?? null;
            $status = $req->status ?? null;

            $perPage = 25;
            $offset = ($page - 1) * $perPage;

            $courses = Course::with(['mentor' => function ($q) {
                $q->select('id', 'uuid', 'name');
            }])
                ->select(['uuid', 'name', 'certificate', "status", 'thumbnail', 'type', 'price', 'level', 'description', 'mentor_id', "created_at", "updated_at"])
                ->when($q, function ($query) use ($q) {
                    $query->whereRaw("lower(name) like '$q'");
                })
                ->when($status, function ($query) use ($status) {
                    $query->where("status", $status);
                })
                ->orderby("created_at", 'desc');

            $total = $courses->count();
            $result = $courses->offset($offset)
                ->limit($perPage)
                ->get()
                ->all();

            $pagination = new LengthAwarePaginator($result, $total, $perPage, $page);
            $pagination->setPath(request()->url());

            return response()->json([
                "status" => "success",
                "metadata" => [
                    'page' => intval($page),
                    'total_page' => ceil($total / $perPage),
                    'per_page' => $pagination->perPage(),

                ],
                "data" => collect($result)->map(function ($row) {
                    return [
                        "uuid" => $row->uuid,
                        "mentor" => [
                            "uuid" => $row->mentor->uuid,
                            "name" => $row->mentor->name,
                        ],
                        "name" => $row->name,
                        "certificate" => $row->certificate,
                        "thumbnail" => $row->thumbnail,
                        "status" => $row->status,
                        "type" => $row->type,
                        "price" => $row->price,
                        "level" => $row->level,
                        "description" => $row->description,
                        "created_at" => $row->created_at,
                        "updated_at" => $row->updated_at,
                    ];
                })
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "name" => "required|string",
                "certificate" => "required|boolean",
                "thumbnail" => "required|url",
                "status" => "required|in:draft,published",
                "type" => "required|in:free,premium",
                "price" => "integer",
                "level" => "required|in:all-level,beginner,intermediate,advance",
                "mentor_uuid" => "required|uuid",
                "description" => "string"
            ];

            $data = $request->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $mentor = Mentor::where("uuid", $request->mentor_uuid)->first();

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => "mentor not found"
                ], 404);
            }

            $course = Course::create([
                'name' => $request->name,
                'uuid' => Str::uuid(),
                "certificate" => $request->certificate,
                "thumbnail" => $request->thumbnail,
                "status" => $request->status,
                "type" => $request->type,
                "price" => $request->price,
                "level" => $request->level,
                "mentor_id" => $mentor->id,
                "description" => $request->description
            ]);

            DB::commit();

            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $course->uuid,
                    "name" => $course->name,
                    "certificate" => $course->certificate,
                    "thumbnail" => $course->thumbnail,
                    "status" => $course->status,
                    "type" => $course->type,
                    "price" => $course->price,
                    "level" => $course->level,
                    "description" => $course->description,
                    "created_at" => $course->created_at,
                    "updated_at" => $course->updated_at,
                    "mentor" => [
                        "uuid" => $course->mentor->uuid,
                        "name" => $course->mentor->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function show($uuid)
    {
        try {
            $validator = Validator::make(['uuid' => $uuid], [
                'uuid' => "required|uuid",
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::with(["reviews" => function ($query) {
                $query->select(["id", "uuid", "user_uuid", "rating", "note"]);
            }], ["studentCourse" => function ($query) {
                $query->select(["id", "course_id", "user_id"]);
            }], ["mentor" => function ($query) {
                $query->select(["id", "uuid", "name", 'created_at', 'updated_at']);
            }], ["images" => function ($query) {
                $query->select(["id", "uuid", "image", 'created_at', 'updated_at']);
            }], ["chapters" => function ($query) {
                $query->with(["lessons" => function ($query2) {
                    $query2->select("id", "uuid", "video");
                }])
                    ->select(["id", "uuid", "name", 'created_at', 'updated_at']);
            }])
                ->where('uuid', $uuid)
                ->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => 'course not found'
                ], 404);
            }


            $reviews = collect($course->reviews)->map(function ($row) {
                $user = json_decode(json_encode(getServiceUser($row->user_uuid), true));
                $userUuid = $user->data->user ? $user->data->user->uuid : null;
                $userName = $user->data->user ? $user->data->user->name : null;

                return [
                    "user" => [
                        "uuid" => $userUuid,
                        "name" => $userName,
                    ],
                    "uuid" => $row->uuid,
                    "name" => $row->name,
                    "created_at" => $row->created_at,
                    "updated_at" => $row->updated_at,
                ];
            });

            $images = collect($course->images)->map(function ($row) {
                return [
                    "uuid" => $row->uuid,
                    "image" => $row->image,
                    "created_at" => $row->created_at,
                    "updated_at" => $row->updated_at,
                ];
            });

            $chapters = collect($course->chapters)->map(function ($row) {
                $lessons = collect($row->lessons)->map(function ($row2) {
                    return [
                        "uuid" => $row2->uuid,
                        "video" => $row2->video,
                    ];
                });

                return [
                    "lessons" => $lessons,
                    "uuid" => $row->uuid,
                    "name" => $row->image,
                    "created_at" => $row->created_at,
                    "updated_at" => $row->updated_at,
                ];
            });

            return response()->json([
                "status" => "success",
                "data" => [
                    "mentor" => [
                        "uuid" => $course->mentor->uuid,
                        "name" => $course->mentor->name,
                        "created_at" => $course->mentor->created_at,
                        "updated_at" => $course->mentor->updated_at,
                    ],
                    "reviews" => $reviews,
                    "images" => $images,
                    "chapters" => $chapters,
                    "total_student" => $course->studentCourse->count(),
                    "uuid" => $course->uuid,
                    "name" => $course->name,
                    "certificate" => $course->certificate,
                    "thumbnail" => $course->thumbnail,
                    "status" => $course->status,
                    "type" => $course->type,
                    "price" => $course->price,
                    "level" => $course->level,
                    "description" => $course->description,
                    "created_at" => $course->created_at,
                    "updated_at" => $course->updated_at,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function update(Request $req, $uuid)
    {
        try {
            DB::beginTransaction();

            $rules = [
                "name" => "string",
                "certificate" => "boolean",
                "thumbnail" => "url",
                "status" => "in:draft,published",
                "type" => "in:free,premium",
                "price" => "integer",
                "level" => "in:all-level,beginner,intermediate,advance",
                "mentor_uuid" => "required|uuid",
                "description" => "string"
            ];

            $data = $req->all();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status" => "error",
                    "message" => $validator->errors()
                ], 400);
            }

            $course = Course::where('uuid', $uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => "course not found"
                ], 404);
            }

            $mentor = Mentor::where("uuid", $req->mentor_uuid)->first();

            if (!$mentor) {
                return response()->json([
                    "status" => "error",
                    "message" => "mentor not found"
                ], 404);
            }

            if ($req->name) $course->name = $req->name;
            if ($req->certificate) $course->certificate = $req->certificate;
            if ($req->thumbnail) $course->thumbnail = $req->thumbnail;
            if ($req->status) $course->status = $req->status;
            if ($req->type) $course->type = $req->type;
            if ($req->price) $course->price = $req->price;
            if ($req->level) $course->level = $req->level;
            if ($req->description) $course->description = $req->description;
            if ($req->mentor_uuid) $course->mentor_id = $mentor->id;

            $course->save();

            DB::commit();
            return response()->json([
                "status" => "success",
                "data" => [
                    "uuid" => $course->uuid,
                    "name" => $course->name,
                    "certificate" => $course->certificate,
                    "thumbnail" => $course->thumbnail,
                    "status" => $course->status,
                    "type" => $course->type,
                    "price" => $course->price,
                    "level" => $course->level,
                    "description" => $course->description,
                    "created_at" => $course->created_at,
                    "updated_at" => $course->updated_at,
                    "mentor" => [
                        "uuid" => $course->mentor->uuid,
                        "name" => $course->mentor->name,
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }

    public function destroy($uuid)
    {
        try {
            DB::beginTransaction();
            $course = Course::where('uuid', $uuid)->first();

            if (!$course) {
                return response()->json([
                    "status" => "error",
                    "message" => 'course not found'
                ], 404);
            }

            $course->delete();

            DB::commit();
            return response()->json([
                "status" => "success",
                "message" => "delete $uuid successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => "error",
                "data" => $e
            ]);
        }
    }
}
