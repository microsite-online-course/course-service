<?php

use Illuminate\Support\Facades\Http;

function getServiceUserById($userUuids = [])
{
    $url = env("SERVICE_USER_URL") . "user/";
    try {
        if (count($userUuids) === 0) {
            return [
                "status" => "success",
                "http_code" => 200,
                "data" => []
            ];
        }

        $res = Http::timeout(10)->get($url, [
            "user_uuids[]" => $userUuids
        ]);

        $data = $res->json();
        $data["http_code"] = $res->getStatusCode();
        return $data;
    } catch (\Throwable $th) {
        return [
            "status" => "error",
            "http_code" => 500,
            "message" => "service user unavailable"
        ];
    }
}
