<?php

use Illuminate\Support\Facades\Http;

function postOrder($params)
{
    $url = env("SERVICE_ORDER_PAYMENT_URL") . "/api/v1/order";
    try {
        $res = Http::timeout(10)->post($url, $params);

        $data = $res->json();
        $data["http_code"] = $res->getStatusCode();
        return $data;
    } catch (\Throwable $th) {
        return [
            "status" => "error",
            "http_code" => 500,
            "message" => "service order unavailable"
        ];
    }
}
