<?php

use Illuminate\Support\Facades\Http;

function getServiceUser($userUuid)
{
    $url = env("SERVICE_USER_URL") . "/api/v1/user/show/$userUuid";

    try {
        $res = Http::timeout(10)->get($url);
        $data = $res->json();
        $data["http_code"] = $res->getStatusCode();
        return $data;
    } catch (\Throwable $th) {
        return [
            "status" => "error",
            "http_code" => 500,
            "message" => "service user unavailable"
        ];
    }
}
