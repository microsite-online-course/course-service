<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCourse extends Model
{
    protected $table = "image_courses";

    protected $fillable = ["uuid", "course_id", "image"];

    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
    ];

    public function course()
    {
        return $this->belongsTo("App\Models\Course");
    }
}
