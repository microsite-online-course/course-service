<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = "chapters";
    protected $fillable = ["uuid", "name", "course_id"];

    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
    ];

    public function course()
    {
        return $this->belongsTo("App\Models\Course");
    }

    public function lessons()
    {
        return $this->belongsTo("App\Models\Lesson")->orderBy("id", "asc");
    }
}
