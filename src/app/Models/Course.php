<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "courses";

    protected $fillable = ["uuid", "name", "certificate", "thumbnail", "type", "status", "price", "level", "description", "mentor_id"];

    protected $cast = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
    ];

    public function mentor()
    {
        return $this->belongsTo("App\Models\Mentor");
    }

    public function chapters()
    {
        return $this->hasMany("App\Models\Chapter")->orderBy("id", "asc");
    }

    public function studentCourse()
    {
        return $this->hasMany("App\Models\MyCourse", 'course_id', 'id')->orderBy("id", "asc");
    }

    public function reviews()
    {
        return $this->hasMany("App\Models\Review")->orderBy("id", "asc");
    }

    public function images()
    {
        return $this->hasMany("App\Models\ImageCourse")->orderBy("id", "asc");
    }
}
